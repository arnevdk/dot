if status is-interactive; and not set -q TMUX; and not test -f /run/.toolboxenv
	exec tmux new-session -A -s main
end
if test -d /usr/local/share/venv
	source /usr/local/share/venv/bin/activate.fish
end
set -Ux EDITOR nvim
