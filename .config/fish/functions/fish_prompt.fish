
function fish_prompt --description 'Write out the prompt'
    set curr_hostname (hostname | cut -d. -f1)
    if test -f /run/.toolboxenv
	set curr_hostname (awk -F'"' '/^name=/{print $2}' /run/.containerenv)
	echo -n (set_color purple)"⬢ "(set_color normal)
    end
    echo -n -s $USER "@"$curr_hostname " " (prompt_pwd) (fish_git_prompt) " > "
end
