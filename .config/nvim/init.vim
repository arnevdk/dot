set nocompatible

let g:ale_completion_enabled = 1

"Install plugins
" Plugins will be downloaded under the specified directory.
call plug#begin(stdpath('data') . '/plugged')

" Syntax checking
Plug 'vim-syntastic/syntastic'
" Linting and fixing
Plug 'w0rp/ale'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Common configuration
" Disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" Encoding
set encoding=utf-8

" Line numbers
set number

" Textwidth
set textwidth=79

" Status line
set statusline=[%n]\ %<%f%h%m

" Buffer navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <Tab> :bn<CR>
nnoremap <S-Tab> :bp<CR>

" Use system clipboard
if has("unnamedplus")
    set clipboard=unnamedplus
else
    set clipboard=unnamed
endif


" Linting and fixing
let g:ale_linters_explicit = 1
let g:ale_linter_aliases = {'tex': ['tex', 'text']}
let g:ale_linters = {
\	'python': ['pylint', 'pydocstyle'],
\	'tex': ['chktex', 'lacheck']
\}
"\	'tex': ['chktex', 'lacheck', 'languagetool']
"" Define LanguageTool linter
"let g:ale_languagetool_executable='/home/arne/.containers/languagetool/languagetool.sh'
"let g:ale_languagetool_options = '--language en-US --mothertongue nl-BE --languagemodel /home/arne/.containers/languagetool/ngrams --enablecategories PUNCTUATION,TYPOGRAPHY,CASING,COLLOCATIONS,CONFUSED_WORDS,CREATIVE_WRITING,GRAMMAR,MISC,MISUSED_TERMS_EU_PUBLICATIONS,NONSTANDARD_PHRASES,PLAIN_ENGLISH,TYPOS,REDUNDANCY,SEMANTICS,STYLE,GENDER_NEUTRALITY,TON_ACADEMIC,TEXT_ANALYSIS --enable AND_ALSO,ARE_ABLE_TO,ARTICLE_MISSING,AS_FAR_AS_X_IS_CONCERNED,BEST_EVER,BLEND_TOGETHER,BRIEF_MOMENT,CAN_NOT,CANT_HELP_BUT,COMMA_WHICH,EG_NO_COMMA,ELLIPSIS,EXACT_SAME,HONEST_TRUTH,HOPEFULLY,IE_NO_COMMA,IN_ORDER_TO,I_VE_A,NEGATE_MEANING,PASSIVE_VOICE,PLAN_ENGLISH,REASON_WHY,SENT_START_NUM,SERIAL_COMMA_OFF,SERIAL_COMMA_ON,SMARTPHONE,THREE_NN,TIRED_INTENSIFIERS,ULESLESS_THAT,WIKIPEDIA,WORLD_AROUND_IT --disable MORFOLOGIK_RULE_EN_US'

let g:ale_fix_on_save = 1
let g:ale_fixers = {
\	'*': ['remove_trailing_lines', 'trim_whitespace'],
\	'tex': ['remove_trailing_lines', 'trim_whitespace'],
\	'python': ['black', 'isort'],
\}
"\	'html': ['prettier', 'remove_trailing_lines', 'trim_whitespace'],
"\	'css': ['prettier', 'remove_trailing_lines', 'trim_whitespace'],
"\	'markdown': ['pandoc', 'remove_trailing_lines'],
"let g:ale_markdown_pandoc_options = '-f markdown -t markdown -s  -'

" Map autocomplete to <TAB>
inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" : "\<TAB>"

filetype on
" LaTeX
au Filetype tex
    \ setlocal tabstop=2 |
    \ setlocal softtabstop=2 |
    \ setlocal shiftwidth=2 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |

" YAML
au Filetype yaml
    \ setlocal tabstop=2 |
    \ setlocal softtabstop=2 |
    \ setlocal shiftwidth=2 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |

" HTML
au Filetype html
    \ setlocal tabstop=2 |
    \ setlocal softtabstop=2 |
    \ setlocal shiftwidth=2 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |

" Python
au Filetype python
    \ setlocal tabstop=4 |
    \ setlocal softtabstop=4 |
    \ setlocal shiftwidth=4 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |

autocmd FileType python nnoremap <leader>b oimport ipdb; ipdb.set_trace()<Esc>
autocmd FileType python nnoremap <leader>B Oimport ipdb; ipdb.set_trace()<Esc>
let python_highlight_all=1
syntax on
autocmd FileType python map <buffer> <F5> :w<CR>:exec '!python' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F5> <esc>:w<CR>:exec '!python' shellescape(@%, 1)<CR>

" JavaScript
au Filetype javascript
    \ setlocal tabstop=4 |
    \ setlocal softtabstop=4 |
    \ setlocal shiftwidth=4 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |


" Markdown
au Filetype markdown
    \ setlocal tabstop=2 |
    \ setlocal softtabstop=2 |
    \ setlocal shiftwidth=2 |
    \ setlocal expandtab |
    \ setlocal autoindent |
    \ setlocal fileformat=unix |

au BufRead,BufNewFile *.jrnl set filetype=markdown
set colorcolumn=80
highlight ColorColumn ctermbg='DarkGrey'

" Spell checking
set spelllang=en_us spell
nnoremap zn ]s
nnoremap zN [s
